﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finanzas.Application.Services.Services;
using NUnit.Framework;
using Moq;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Models;
using Finanzas.Domain.Entities;

namespace Finanzas.Application.Services.Test.Services
{
    [TestFixture]
    public class TipoMovimientoServiceTest
    {
        private Mock<IBaseRepository<TiposMovimiento,TiposMovimientoModel>> _iTipoMovimientoRepositoryMock;

        [SetUp]
        public void Initial()
        {
            _iTipoMovimientoRepositoryMock = new Mock<IBaseRepository<TiposMovimiento, TiposMovimientoModel>>();
        }

        [TearDown]
        public void Finalize()
        {
            _iTipoMovimientoRepositoryMock = null;
        }

        [Test]
        public void Then_Have_One_Data_GetAll_Return_One_Data_In_Collection()
        {
            //Arrage
            var listaDeResultados = new List<TiposMovimientoModel>();
            listaDeResultados.Add(new TiposMovimientoModel());

            _iTipoMovimientoRepositoryMock
                .Setup(p => p.GetAll())
                .Returns(() => listaDeResultados);


            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetAll();
            Assert.IsNotEmpty(res.Object, "La lista no debe ser vacia.");
            Assert.AreEqual(1, res.Object.Count(), "La lista deberia tener un solo valor en este caso.");
        }

        [Test]
        public void Then_Not_Have_Data_GetAll_Return_Emply_Collection()
        {
            //Arrage

            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetAll();
            Assert.IsEmpty(res.Object, "La lista debe estar vacia.");
            Assert.AreEqual(0, res.Object.Count(), "La lista no deberia tener valor en este caso.");
        }

        [Test]
        public void Then_Throw_Exception_GetAll_Return_Null()
        {
            //Arrage

            _iTipoMovimientoRepositoryMock
                .Setup(p => p.GetAll())
                .Throws<Exception>();

            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetAll();

            Assert.IsNull(res.Object, "La lista debe estar vacia.");
        }


        [Test]
        public void Then_Have_One_Data_GetActives_Return_One_Data_In_Collection()
        {
            //Arrage
            var listaDeResultados = new List<TiposMovimientoModel>();
            listaDeResultados.Add(new TiposMovimientoModel());

            _iTipoMovimientoRepositoryMock
                .Setup(p => p.GetWhere(It.IsAny<Func<TiposMovimiento, bool>>()))
                .Returns(() => listaDeResultados);


            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetActives();
            Assert.IsNotEmpty(res.Object, "La lista no debe ser vacia.");
            Assert.AreEqual(1, res.Object.Count(), "La lista deberia tener un solo valor en este caso.");
        }

        [Test]
        public void Then_Not_Have_Data_GetActives_Return_Emply_Collection()
        {
            //Arrage

            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetActives();
            Assert.IsEmpty(res.Object, "La lista debe estar vacia.");
            Assert.AreEqual(0, res.Object.Count(), "La lista no deberia tener valor en este caso.");
        }

        [Test]
        public void Then_Throw_Exception_GetActives_Return_Null()
        {
            //Arrage

            _iTipoMovimientoRepositoryMock
                .Setup(p => p.GetWhere(It.IsAny<Func<TiposMovimiento, bool>>()))
                .Throws<Exception>();

            var tipoMovimientoService = new TipoMovimientoService(_iTipoMovimientoRepositoryMock.Object);

            //Act
            var res = tipoMovimientoService.GetActives();

            Assert.IsNull(res.Object, "La lista debe estar vacia.");
        }

    }
}
