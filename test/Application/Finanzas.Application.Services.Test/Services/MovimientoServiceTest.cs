﻿using Finanzas.Application.Services.Services;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Models;
using FluentValidation;
using FluentValidation.Results;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Test.Services
{
    public class MovimientoServiceTest
    {
        private Mock<IBaseRepository<Movimiento,MovimientoModel>> _iMovimientoRepositoryMock;
        private Mock<IBaseRepository<Cuenta,CuentaModel>> _iCuentaRepositoryMock;

        [SetUp]
        public void Initial()
        {
            _iMovimientoRepositoryMock = new Mock<IBaseRepository<Movimiento,MovimientoModel>>();
            _iCuentaRepositoryMock = new Mock<IBaseRepository<Cuenta,CuentaModel>>();
        }

        [TearDown]
        public void Finalize()
        {
            _iMovimientoRepositoryMock = null;
            _iCuentaRepositoryMock = null;
        }

        [Test]
        public void Then_Have_One_Data_GetAll_Return_One_Data_In_Collection()
        {
            //Arrage
            var listaDeResultados = new List<MovimientoModel>();
            listaDeResultados.Add(new MovimientoModel());

            _iMovimientoRepositoryMock
                .Setup(p => p.GetAll())
                .Returns(() => listaDeResultados);

            
            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetAll();
            Assert.IsNotEmpty(res.Object, "La lista no debe ser vacia.");
            Assert.AreEqual(1, res.Object.Count(), "La lista deberia tener un solo valor en este caso.");
        }

        [Test]
        public void Then_Not_Have_Data_GetAll_Return_Emply_Collection()
        {
            //Arrage

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetAll();
            Assert.IsEmpty(res.Object, "La lista debe estar vacia.");
            Assert.AreEqual(0, res.Object.Count(), "La lista no deberia tener valor en este caso.");
        }

        [Test]
        public void Then_Throw_Exception_GetAll_Return_Null()
        {
            //Arrage

            _iMovimientoRepositoryMock
                .Setup(p => p.GetAll())
                .Throws<Exception>();

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetAll();

            Assert.IsNull(res.Object, "La lista debe estar vacia.");
        }


  

        [Test]
        public void Then_Have_Data_GetById_Return_Data()
        {
            //Arrage
            var result = new MovimientoModel();

            _iMovimientoRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(() => result);


            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetById(It.IsAny<int>());
            Assert.IsNotNull(res.Object, "El objeto no debe ser nulo.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }

        [Test]
        public void Then_Not_Have_Data_GetById_Return_Null_Object()
        {
            //Arrage

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetById(1);
            Assert.IsNull(res.Object, "El objeto debe ser nulo.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }

        [Test]
        public void Then_Throw_Exception_GetById_Return_Null()
        {
            //Arrage

            _iMovimientoRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetById(0);

            Assert.IsNull(res.Object, "El objeto debe ser nulo.");
            Assert.IsFalse(res.Ok, "El resultado debe ser falso.");
        }
        [Test]
        public void Then_Delete_Data_Return_True()
        {

            _iMovimientoRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Returns(true);

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsTrue(res.Object, "El resultado debe ser true.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");

        }

        [Test]
        public void Then_No_Delete_Data_Return_False()
        {

            _iMovimientoRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Returns(false);

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsFalse(res.Object, "El resultado debe ser false.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }


        [Test]
        public void Then_Throw_Exception_delete_Return_False()
        {

            _iMovimientoRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Throws<Exception>();

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsFalse(res.Object, "El resultado debe ser false.");
            Assert.IsFalse(res.Ok, "El resultado debe ser false.");
        }


        // Validador ok y la db ok
        [Test]
        public void Then_NoCredit_Validator_Ok_Db_Ok_Insert_Return_Ok()
        {

            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(true);

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel());

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsTrue(res.Object);

            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Once);



        }


        // Validador ok y la db no
        [Test]
        public void Then_NoCredit_Validator_Ok_Db_Not_Ok_Insert_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(false);

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel());

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Once);
        }


        // Validador not ok
        [Test]
        public void Then_NoCredit_Validator_Not_Ok_Insert_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("Nombre", "Nombre no debe ser nulo") }));

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Insert(obj);

            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Never);
        }

        //Exepcion

        [Test]
        public void Then_NoCredit_Throw_Exepction_Insert_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Throws<Exception>();

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel());

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Insert(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Once);
        }


        [Test]
        public void Then_Credit_Validator_Ok_Db_Ok_Insert_Return_Ok()
        {

            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(true);

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel() { EsTarjetaCredito=true });

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            obj.CatidadCuotas = 3;
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsTrue(res.Object);

            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.AtLeastOnce);



        }


        [Test]
        public void Then_Credit_With_Not_Cuotas_Return_Error()
        {

            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(true);

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel() { EsTarjetaCredito = true });

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("CatidadCuotas", "El cantidad de cuotas debe ser mayor a 0") }));

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Insert(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);

            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Never);
        }


        // Validador ok y la db no
        [Test]
        public void Then_Credit_Validator_Ok_Db_Not_Ok_Insert_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Returns(false);

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel() { EsTarjetaCredito = true });

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            obj.CatidadCuotas = 3;
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.AtLeastOnce);
        }
     
        //Exepcion

        [Test]
        public void Then_Credit_Throw_Exepction_Insert_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Insert(It.IsAny<MovimientoModel>()))
              .Throws<Exception>();

            _iCuentaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CuentaModel() { EsTarjetaCredito = true });

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            obj.CatidadCuotas = 3;
            var res = categoriaService.Insert(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Insert(obj), Times.Once);
        }




        // Validador ok y la db ok
        [Test]
        public void Then_Validator_Ok_Db_Ok_Update_Return_Ok()
        {

            _iMovimientoRepositoryMock
              .Setup(p => p.Update(It.IsAny<MovimientoModel>()))
              .Returns(true);

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Update(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsTrue(res.Object);

            _iMovimientoRepositoryMock.Verify(p => p.Update(obj), Times.Once);



        }


        // Validador ok y la db no
        [Test]
        public void Then_Validator_Ok_Db_Not_Ok_Update_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Update(It.IsAny<MovimientoModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Update(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Update(obj), Times.Once);
        }


        // Validador not ok
        [Test]
        public void Then_Validator_Not_Ok_Insert_Update_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Update(It.IsAny<MovimientoModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("Nombre", "Nombre no debe ser nulo") }));

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Update(obj);

            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Update(obj), Times.Never);
        }

        //Exepcion

        [Test]
        public void Then_Throw_Exepction_Update_Return_False()
        {
            _iMovimientoRepositoryMock
              .Setup(p => p.Update(It.IsAny<MovimientoModel>()))
              .Throws<Exception>();

            var validatorMock = new Mock<IValidator<MovimientoModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<MovimientoModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new MovimientoService(_iMovimientoRepositoryMock.Object, _iCuentaRepositoryMock.Object, validatorMock.Object);

            var obj = new MovimientoModel();
            var res = categoriaService.Update(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iMovimientoRepositoryMock.Verify(p => p.Update(obj), Times.Once);
        }







    }
}
