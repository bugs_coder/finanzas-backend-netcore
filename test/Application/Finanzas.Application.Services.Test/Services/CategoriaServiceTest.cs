﻿using Finanzas.Application.Services.Services;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Models;
using FluentValidation;
using FluentValidation.Results;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Test.Services
{
    public class CategoriaServiceTest
    {

        private Mock<IBaseRepository<Categoria,CategoriaModel>> _iCategoriaRepositoryMock;

        [SetUp]
        public void Initial()
        {
            _iCategoriaRepositoryMock = new Mock<IBaseRepository<Categoria,CategoriaModel>>();
        }

        [TearDown]
        public void Finalize()
        {
            _iCategoriaRepositoryMock = null;
        }

        [Test]
        public void Then_Have_One_Data_GetAll_Return_One_Data_In_Collection()
        {
            //Arrage
            var listaDeResultados = new List<CategoriaModel>();
            listaDeResultados.Add(new CategoriaModel());

            _iCategoriaRepositoryMock
                .Setup(p => p.GetAll())
                .Returns(() => listaDeResultados);

            
            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object,null);

            //Act
            var res = categoriaService.GetAll();
            Assert.IsNotEmpty(res.Object, "La lista no debe ser vacia.");
            Assert.AreEqual(1, res.Object.Count(), "La lista deberia tener un solo valor en este caso.");
        }

        [Test]
        public void Then_Not_Have_Data_GetAll_Return_Emply_Collection()
        {
            //Arrage

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetAll();
            Assert.IsEmpty(res.Object, "La lista debe estar vacia.");
            Assert.AreEqual(0, res.Object.Count(), "La lista no deberia tener valor en este caso.");
        }

        [Test]
        public void Then_Throw_Exception_GetAll_Return_Null()
        {
            //Arrage

            _iCategoriaRepositoryMock
                .Setup(p => p.GetAll())
                .Throws<Exception>();

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetAll();

            Assert.IsNull(res.Object, "La lista debe estar vacia.");
        }


        [Test]
        public void Then_Have_One_Data_GetActives_Return_One_Data_In_Collection()
        {
            //Arrage
            var listaDeResultados = new List<CategoriaModel>();
            listaDeResultados.Add(new CategoriaModel());

            _iCategoriaRepositoryMock
                .Setup(p => p.GetWhere(It.IsAny<Func<Categoria, bool>>()))
                .Returns(() => listaDeResultados);


            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetActives();
            Assert.IsNotEmpty(res.Object, "La lista no debe ser vacia.");
            Assert.AreEqual(1, res.Object.Count(), "La lista deberia tener un solo valor en este caso.");
        }

        [Test]
        public void Then_Not_Have_Data_GetActives_Return_Emply_Collection()
        {
            //Arrage

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetActives();
            Assert.IsEmpty(res.Object, "La lista debe estar vacia.");
            Assert.AreEqual(0, res.Object.Count(), "La lista no deberia tener valor en este caso.");
        }

        [Test]
        public void Then_Throw_Exception_GetActives_Return_Null()
        {
            //Arrage

            _iCategoriaRepositoryMock
                .Setup(p => p.GetWhere(It.IsAny<Func<Categoria, bool>>()))
                .Throws<Exception>();

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetActives();

            Assert.IsNull(res.Object, "La lista debe estar vacia.");
        }

        [Test]
        public void Then_Have_Data_GetById_Return_Data()
        {
            //Arrage
            var result = new CategoriaModel();

            _iCategoriaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(() => result);


            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetActives();
            Assert.IsNotNull(res.Object, "El objeto no debe ser nulo.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }

        [Test]
        public void Then_Not_Have_Data_GetById_Return_Null_Object()
        {
            //Arrage

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetById(1);
            Assert.IsNull(res.Object, "El objeto debe ser nulo.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }

        [Test]
        public void Then_Throw_Exception_GetById_Return_Null()
        {
            //Arrage

            _iCategoriaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.GetById(0);

            Assert.IsNull(res.Object, "El objeto debe ser nulo.");
            Assert.IsFalse(res.Ok, "El resultado debe ser falso.");
        }
        [Test]
        public void Then_Delete_Data_Return_True()
        {

            _iCategoriaRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Returns(true);

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsTrue(res.Object, "El resultado debe ser true.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");

        }

        [Test]
        public void Then_No_Delete_Data_Return_False()
        {

            _iCategoriaRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Returns(false);

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsFalse(res.Object, "El resultado debe ser false.");
            Assert.IsTrue(res.Ok, "El resultado debe ser true.");
        }


        [Test]
        public void Then_Throw_Exception_delete_Return_False()
        {

            _iCategoriaRepositoryMock
             .Setup(p => p.Delete(It.IsAny<int>()))
             .Throws<Exception>();

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, null);

            //Act
            var res = categoriaService.Delete(0);


            Assert.IsFalse(res.Object, "El resultado debe ser false.");
            Assert.IsFalse(res.Ok, "El resultado debe ser false.");
        }


        // Validador ok y la db ok
        [Test]
        public void Then_Validator_Ok_Db_Ok_Insert_Return_Ok()
        {

            _iCategoriaRepositoryMock
              .Setup(p => p.Insert(It.IsAny<CategoriaModel>()))
              .Returns(true);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsTrue(res.Object);

            _iCategoriaRepositoryMock.Verify(p => p.Insert(obj), Times.Once);



        }


        // Validador ok y la db no
        [Test]
        public void Then_Validator_Ok_Db_Not_Ok_Insert_Return_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Insert(It.IsAny<CategoriaModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Insert(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Insert(obj), Times.Once);
        }


        // Validador not ok
        [Test]
        public void Then_Validator_Not_Ok_Insert_Return_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Insert(It.IsAny<CategoriaModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("Nombre", "Nombre no debe ser nulo") }));

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Insert(obj);

            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Insert(obj), Times.Never);
        }

        //Exepcion

        [Test]
        public void Then_Throw_Exepction_Insert_Return_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Insert(It.IsAny<CategoriaModel>()))
              .Throws<Exception>();

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Insert(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Insert(obj), Times.Once);
        }


        // Validador ok y la db ok
        [Test]
        public void Then_Validator_Ok_Db_Ok_Update_Return_Ok()
        {

            _iCategoriaRepositoryMock
              .Setup(p => p.Update(It.IsAny<CategoriaModel>()))
              .Returns(true);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Update(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsTrue(res.Object);

            _iCategoriaRepositoryMock.Verify(p => p.Update(obj), Times.Once);



        }


        // Validador ok y la db no
        [Test]
        public void Then_Validator_Ok_Db_Not_Ok_Update_Return_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Update(It.IsAny<CategoriaModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Update(obj);
            Assert.IsTrue(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Update(obj), Times.Once);
        }


        // Validador not ok
        [Test]
        public void Then_Validator_Not_Ok_Insert_Update_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Update(It.IsAny<CategoriaModel>()))
              .Returns(false);

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("Nombre", "Nombre no debe ser nulo") }));

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Update(obj);

            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Update(obj), Times.Never);
        }

        //Exepcion

        [Test]
        public void Then_Throw_Exepction_Update_Return_False()
        {
            _iCategoriaRepositoryMock
              .Setup(p => p.Update(It.IsAny<CategoriaModel>()))
              .Throws<Exception>();

            var validatorMock = new Mock<IValidator<CategoriaModel>>();

            validatorMock
                .Setup(p => p.Validate(It.IsAny<CategoriaModel>()))
                .Returns(new ValidationResult());

            var categoriaService = new CategoriaService(_iCategoriaRepositoryMock.Object, validatorMock.Object);

            var obj = new CategoriaModel();
            var res = categoriaService.Update(obj);
            Assert.IsFalse(res.Ok);
            Assert.IsFalse(res.Object);
            _iCategoriaRepositoryMock.Verify(p => p.Update(obj), Times.Once);
        }







    }
}
