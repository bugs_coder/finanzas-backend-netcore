﻿using Finanzas.Domain.Models;
using Finanzas.Intrastructure.FluentAPI.Validators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.TestHelper;
using Finanzas.Domain.Interfaces.Repositories;
using Moq;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Application.Services.Builders;
using Finanzas.Domain.Constants;

namespace Finanzas.Intrastructure.FluentAPI.Test.Validators
{
    [TestFixture]
    public class MovimientoValidatorTest
    {
        private MovimientoValidator _validator;
        private Mock<IBaseRepository<Cuenta,CuentaModel>> _iCuentaRepositoryMock;
        private Mock<IBaseRepository<Categoria,CategoriaModel>> _iCategoriaRepositoryMock;
        private IErrorMessageBuilder _iErrorMessageBuilder;

        [SetUp]
        public void Initial()
        {

            _iCuentaRepositoryMock = new Mock<IBaseRepository<Cuenta,CuentaModel>>();

            _iCategoriaRepositoryMock = new Mock<IBaseRepository<Categoria,CategoriaModel>>();
            _iErrorMessageBuilder = new ErrorMessageBuilder();

            _validator = new MovimientoValidator(_iCuentaRepositoryMock.Object, _iCategoriaRepositoryMock.Object, _iErrorMessageBuilder);

        }

        [TearDown]
        public void Finalize()
        {
            _iCuentaRepositoryMock = null;
            _iCategoriaRepositoryMock = null;
            _validator = null;
        }


        [Test]
        public void Then_IdCategoria_Is_Valid_Validator_return_Ok()
        {

            _iCategoriaRepositoryMock
                .Setup(p => p.GetById(It.IsAny<int>()))
                .Returns(new CategoriaModel() { Id = 1 });

            var obj = new MovimientoModel();
            obj.IdCategoria = 1;
            var res = _validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.IdCategoria);
        }

        [Test]
        public void Then_IdCategoria_Is_Not_Valid_Validator_return_Error()
        {
            var obj = new MovimientoModel();
            obj.IdCategoria = 0;
            var res = _validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.IdCategoria);
            var errors = res.Errors;
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(_iErrorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "IdCategoria", "0")
                            .Build())));

        }


        [Test]
        public void Then_IdCuenta_Is_Valid_Validator_return_Ok()
        {
            _iCuentaRepositoryMock
              .Setup(p => p.GetById(It.IsAny<int>()))
              .Returns(new CuentaModel() { Id = 1 });

            var obj = new MovimientoModel();
            obj.IdCuenta = 1;
            var res = _validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.IdCuenta);
        }

        [Test]
        public void Then_IdCuenta_Is_Not_Valid_Validator_return_Error()
        {
            var obj = new MovimientoModel();
            obj.IdCategoria = 0;
            var res = _validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.IdCuenta);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "IdCuenta", "0")
                            .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));

        }


        [Test]
        public void Then_IdTipoMovimiento_Is_Valid_Validator_return_Ok()
        {
            var obj = new MovimientoModel();
            obj.IdTipoMovimiento = 1;
            var res = _validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.IdTipoMovimiento);
        }

        [Test]
        public void Then_IdTipoMovimiento_Is_Not_Valid_Validator_return_Error()
        {
            var obj = new MovimientoModel();
            obj.IdTipoMovimiento = 0;
            var res = _validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.IdTipoMovimiento);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "IdTipoMovimiento", "0")
                .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));

        }


        [Test]
        public void Then_Monto_Is_Valid_Validator_return_Ok()
        {
            var obj = new MovimientoModel();
            obj.Monto = 100;
            var res = _validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.Monto);
        }

        [Test]
        public void Then_Monto_Is_Not_Valid_Validator_return_Error()
        {
            var obj = new MovimientoModel();
            obj.Monto = 0;
            var res = _validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Monto);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                 .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                 .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "Monto", "0")
                 .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));

        }


    }
}
