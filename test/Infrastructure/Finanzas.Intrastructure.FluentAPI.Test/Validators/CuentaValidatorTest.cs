﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FluentValidation.TestHelper;
using Finanzas.Intrastructure.FluentAPI.Validators;
using Finanzas.Domain.Models;
using Moq;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Application.Services.Builders;
using Finanzas.Domain.Constants;

namespace Finanzas.Intrastructure.FluentAPI.Test.Validators
{
    [TestFixture]
    public class CuentaValidatorTest
    {

        private IErrorMessageBuilder _iErrorMessageBuilder;

        [SetUp]
        public void Initial()
        {
            _iErrorMessageBuilder = new ErrorMessageBuilder();

        }


        [Test]
        public void Then_Nombre_Is_Valid_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);

            var obj = new CuentaModel();
            obj.Nombre = "nombre valido";
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.Nombre);
        }

        [Test]
        public void Then_Nombre_Is_Null_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);

            var obj = new CuentaModel();
            obj.Nombre = null;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
            .WithErrorCode(ErrorCodesConstant.COD_NOTNULL)
            .WithErrorMessage(ErrorMessagesConstant.MSG_NOTNULL, "Nombre")
            .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }

        [Test]
        public void Then_Nombre_Is_Empty_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.Nombre = "";
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                .WithErrorCode(ErrorCodesConstant.COD_EMPTY)
                .WithErrorMessage(ErrorMessagesConstant.MSG_EMPTY, "Nombre")
                .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }


        [Test]
        public void Then_Nombre_Is_Menor_10_Characters_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.Nombre = "12345";
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
               .WithErrorCode(ErrorCodesConstant.COD_MINIMO)
               .WithErrorMessage(ErrorMessagesConstant.MSG_MINIMO, "Nombre", "10")
               .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));


        }


        [Test]
        public void Then_Nombre_Is_Mayor_150_Characters_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.Nombre = string.Join("", Enumerable.Repeat("1234567890.", 15));
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
               .WithErrorCode(ErrorCodesConstant.COD_MAXIMO)
               .WithErrorMessage(ErrorMessagesConstant.MSG_MAXIMO, "Nombre", "150")
               .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }
        [Test]
        public void Then_Id_Is_Valid_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.Id = 1;
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.Id);
        }


        [Test]
        public void Then_Id_Is_Menor_0_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.Id = -1;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Id);
        }


        [Test]
        public void Then_DiaCierre_Is_Null_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();

            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.DiaCierre);
        }

        [Test]
        public void Then_DiaCierre_Is_Valid_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaCierre = 10;
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.DiaCierre);
        }


        [Test]
        public void Then_DiaCierre_Is_Menor_1_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaCierre = 0;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.DiaCierre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
               .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
               .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de cierre", "1", "28")
               .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));

        }


        [Test]
        public void Then_DiaCierre_Is_Mayor_28_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaCierre = 30;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.DiaCierre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
      .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
      .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de cierre", "1", "28")
      .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }




        [Test]
        public void Then_DiaVencimiento_Is_Null_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();

            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.DiaVencimiento);
        }

        [Test]
        public void Then_DiaVencimiento_Is_Valid_Validator_return_Ok()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaVencimiento = 10;
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.DiaVencimiento);
        }


        [Test]
        public void Then_DiaVencimiento_Is_Menor_1_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaVencimiento = 0;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.DiaVencimiento);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                     .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
                     .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de vencimiento", "1", "28")
                     .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));

        }


        [Test]
        public void Then_DiaVencimiento_Is_Mayor_28_Validator_return_Error()
        {
            var validator = new CuentaValidator(_iErrorMessageBuilder);
            var obj = new CuentaModel();
            obj.DiaVencimiento = 30;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.DiaVencimiento);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                           .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
                           .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de vencimiento", "1", "28")
                           .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }





    }
}
