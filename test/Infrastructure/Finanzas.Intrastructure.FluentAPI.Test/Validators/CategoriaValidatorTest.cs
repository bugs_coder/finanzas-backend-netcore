﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FluentValidation.TestHelper;
using Finanzas.Intrastructure.FluentAPI.Validators;
using Finanzas.Domain.Models;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Application.Services.Builders;
using Moq;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Constants;

namespace Finanzas.Intrastructure.FluentAPI.Test.Validators
{
    [TestFixture]
    public class CategoriaValidatorTest
    {
        private Mock<IBaseRepository<TiposMovimiento, TiposMovimientoModel>> _iTipoMovimientoRepositoryMock;

        private IErrorMessageBuilder _iErrorMessageBuilder;

        [SetUp]
        public void Initial()
        {
            _iTipoMovimientoRepositoryMock = new Mock<IBaseRepository<TiposMovimiento, TiposMovimientoModel>>();
            _iTipoMovimientoRepositoryMock.Setup(p => p.GetById(It.IsAny<int>())).Returns(new TiposMovimientoModel());


            _iErrorMessageBuilder = new ErrorMessageBuilder();

        }

        [Test]
        public void Then_Nombre_Is_Valid_Validator_return_Ok()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);

            var obj = new CategoriaModel();
            obj.Nombre = "nombre valido";
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.Nombre);
        }

        [Test]
        public void Then_Nombre_Is_Null_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);

            var obj = new CategoriaModel();
            obj.Nombre = null;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
          .WithErrorCode(ErrorCodesConstant.COD_NOTNULL)
          .WithErrorMessage(ErrorMessagesConstant.MSG_NOTNULL, "Nombre")
          .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }

        [Test]
        public void Then_Nombre_Is_Empty_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.Nombre = "";
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
               .WithErrorCode(ErrorCodesConstant.COD_EMPTY)
               .WithErrorMessage(ErrorMessagesConstant.MSG_EMPTY, "Nombre")
               .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }


        [Test]
        public void Then_Nombre_Is_Menor_10_Characters_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.Nombre = "12345";
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                .WithErrorCode(ErrorCodesConstant.COD_MINIMO)
                .WithErrorMessage(ErrorMessagesConstant.MSG_MINIMO, "Nombre", "10")
                .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }


        [Test]
        public void Then_Nombre_Is_Mayor_150_Characters_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.Nombre = string.Join("", Enumerable.Repeat("1234567890.", 15));
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Nombre);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                     .WithErrorCode(ErrorCodesConstant.COD_MAXIMO)
                     .WithErrorMessage(ErrorMessagesConstant.MSG_MAXIMO, "Nombre", "150")
                     .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }
        [Test]
        public void Then_Id_Is_Valid_Validator_return_Ok()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.Id = 1;
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.Id);
        }


        [Test]
        public void Then_Id_Is_Menor_0_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.Id = -1;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.Id);
        }


        [Test]
        public void Then_IdTipoMovimiento_Is_Valid_Validator_return_Ok()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.IdTipoMovimiento = 1;
            var res = validator.TestValidate(obj);
            res.ShouldNotHaveValidationErrorFor(o => o.IdTipoMovimiento);
        }


        [Test]
        public void Then_IdTipoMovimiento_Is_0_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.IdTipoMovimiento = 0;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.IdTipoMovimiento);
            var errors = res.Errors;
            var msg = _iErrorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "tipo de movimiento", "0")
                            .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }

        [Test]
        public void Then_IdTipoMovimiento_Menor_0_Validator_return_Error()
        {
            var validator = new CategoriaValidator(_iErrorMessageBuilder, _iTipoMovimientoRepositoryMock.Object);
            var obj = new CategoriaModel();
            obj.IdTipoMovimiento = -1;
            var res = validator.TestValidate(obj);
            res.ShouldHaveValidationErrorFor(o => o.IdTipoMovimiento);
            var errors = res.Errors; var msg = _iErrorMessageBuilder
                      .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                      .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "tipo de movimiento", "0")
                      .Build();
            Assert.IsTrue(errors.Any(p => p.ErrorMessage.Equals(msg)));
        }


    }
}
