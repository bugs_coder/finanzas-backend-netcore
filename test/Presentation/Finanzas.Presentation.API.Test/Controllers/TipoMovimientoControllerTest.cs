﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Finanzas.Presentation.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Finanzas.Presentation.API.Test.Controllers
{

    [TestFixture]
    public class TipoMovimientoControllerTest
    {
        [Test]
        public void Then_Data_Is_Ok_GetAll_Return_HTTPCODE_200()
        {

            //Arrage
            var data = new ResponseModel<IEnumerable<TiposMovimientoModel>>();
            
            var iTipoMovimientoServicesMock = new Mock<ITipoMovimientoService>();

            iTipoMovimientoServicesMock.Setup(p => p.GetAll()).Returns(data);


            var controller = new TipoMovimientoController(iTipoMovimientoServicesMock.Object);
            //Act
            var res = controller.GetAll() as ObjectResult;

            var obj = res.Value as ResponseModel<IEnumerable<TiposMovimientoModel>>;

            //Assert
            Assert.AreEqual(200, res.StatusCode);
            Assert.True(obj.Ok);
        }

        [Test]
        public void Then_Data_Is_Exception_GetAll_Return_HTTPCODE_500()
        {

            //Arrage
            var data = new ResponseModel<IEnumerable<TiposMovimientoModel>>();
            data.AddSystemErrors("error para el test.");

            var iTipoMovimientoServicesMock = new Mock<ITipoMovimientoService>();

            iTipoMovimientoServicesMock.Setup(p => p.GetAll()).Returns(data);


            var controller = new TipoMovimientoController(iTipoMovimientoServicesMock.Object);
            //Act
            var res = controller.GetAll() as ObjectResult;
            var obj = res.Value as ResponseModel<IEnumerable<TiposMovimientoModel>>;

            //Assert
            Assert.AreEqual(500, res.StatusCode);
            Assert.False(obj.Ok);
        }

        [Test]
        public void Then_Data_Is_Ok_GetActives_Return_HTTPCODE_200()
        {

            //Arrage
            var data = new ResponseModel<IEnumerable<TiposMovimientoModel>>();

            var iTipoMovimientoServicesMock = new Mock<ITipoMovimientoService>();

            iTipoMovimientoServicesMock.Setup(p => p.GetActives()).Returns(data);


            var controller = new TipoMovimientoController(iTipoMovimientoServicesMock.Object);
            //Act
            var res = controller.GetActives() as ObjectResult;

            var obj = res.Value as ResponseModel<IEnumerable<TiposMovimientoModel>>;

            //Assert
            Assert.AreEqual(200, res.StatusCode);
            Assert.True(obj.Ok);
        }

        [Test]
        public void Then_Data_Is_Exception_GetActives_Return_HTTPCODE_500()
        {

            //Arrage
            var data = new ResponseModel<IEnumerable<TiposMovimientoModel>>();
            data.AddSystemErrors("error para el test.");

            var iTipoMovimientoServicesMock = new Mock<ITipoMovimientoService>();

            iTipoMovimientoServicesMock.Setup(p => p.GetActives()).Returns(data);


            var controller = new TipoMovimientoController(iTipoMovimientoServicesMock.Object);
            //Act
            var res = controller.GetActives() as ObjectResult;
            var obj = res.Value as ResponseModel<IEnumerable<TiposMovimientoModel>>;

            //Assert
            Assert.AreEqual(500, res.StatusCode);
            Assert.False(obj.Ok);
        }
    }
}
