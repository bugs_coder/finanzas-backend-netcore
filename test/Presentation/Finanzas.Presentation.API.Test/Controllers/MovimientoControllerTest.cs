﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Finanzas.Presentation.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Finanzas.Presentation.API.Test.Controllers
{
    [TestFixture]
    public class MovimientoControllerTest
    {
        [Test]
        public void Then_Data_Is_Ok_GetAll_Return_HTTPCODE_200()
        {

            var data = new ResponseModel<IEnumerable<MovimientoModel>>();

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.GetAll()).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.GetAll() as ObjectResult;

            var obj = res.Value as ResponseModel<IEnumerable<MovimientoModel>>;

            Assert.AreEqual(200, res.StatusCode);
            Assert.IsTrue(obj.Ok);

        }

        [Test]
        public void Then_Data_Is_Not_Ok_GetAll_Return_HTTPCODE_500()
        {

            var data = new ResponseModel<IEnumerable<MovimientoModel>>();
            data.AddSystemErrors("Error para test.");
            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.GetAll()).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.GetAll() as ObjectResult;

            var obj = res.Value as ResponseModel<IEnumerable<MovimientoModel>>;

            Assert.AreEqual(500, res.StatusCode);
            Assert.IsFalse(obj.Ok);

        }

        [Test]
        public void Then_Data_Is_Ok_GetById_Return_HTTPCODE_200()
        {

            var data = new ResponseModel<MovimientoModel>();

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.GetById(It.IsAny<int>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.GetById(It.IsAny<int>()) as ObjectResult;

            var obj = res.Value as ResponseModel<MovimientoModel>;

            Assert.AreEqual(200, res.StatusCode);
            Assert.IsTrue(obj.Ok);

        }

        [Test]
        public void Then_Data_Is_Not_Ok_GetById_Return_HTTPCODE_500()
        {

            var data = new ResponseModel<MovimientoModel>();
            data.AddSystemErrors("Error para test.");
            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.GetById(It.IsAny<int>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.GetById(It.IsAny<int>()) as ObjectResult;

            var obj = res.Value as ResponseModel<MovimientoModel>;

            Assert.AreEqual(500, res.StatusCode);
            Assert.IsFalse(obj.Ok);

        }


        [Test]
        public void Then_Data_Is_Ok_Delete_Return_HTTPCODE_200()
        {

            var data = new ResponseModel<bool>();

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Delete(It.IsAny<int>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Delete(It.IsAny<int>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(200, res.StatusCode);
            Assert.IsTrue(obj.Ok);

        }

        [Test]
        public void Then_Data_Is_Not_Ok_Delete_Return_HTTPCODE_500()
        {

            var data = new ResponseModel<bool>();
            data.AddSystemErrors("Error para test.");

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Delete(It.IsAny<int>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Delete(It.IsAny<int>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;
            Assert.AreEqual(500, res.StatusCode);
            Assert.IsFalse(obj.Ok);

        }


        [Test]
        public void Then_Data_Is_Ok_Insert_Return_HTTPCODE_200()
        {

            var data = new ResponseModel<bool>();

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Insert(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Insert(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(200, res.StatusCode);
            Assert.IsTrue(obj.Ok);

        }


        [Test]
        public void Then_Input_Is_Not_Ok_Insert_Return_HTTPCODE_400()
        {

            var data = new ResponseModel<bool>();
            data.AddBussinessErrors("error para test");
            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Insert(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Insert(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(400, res.StatusCode);
            Assert.IsFalse(obj.Ok);
        }

        [Test]
        public void Then_Data_Is_Not_Ok_Insert_Return_HTTPCODE_500()
        {

            var data = new ResponseModel<bool>();
            data.AddSystemErrors("Error para test.");

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Insert(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Insert(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(500, res.StatusCode);

            Assert.IsFalse(obj.Ok);

        }




        [Test]
        public void Then_Data_Is_Ok_Update_Return_HTTPCODE_200()
        {

            var data = new ResponseModel<bool>();

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Update(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Update(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(200, res.StatusCode);
            Assert.IsTrue(obj.Ok);

        }


        [Test]
        public void Then_Input_Is_Not_Ok_Update_Return_HTTPCODE_400()
        {

            var data = new ResponseModel<bool>();
            data.AddBussinessErrors("error para test");
            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Update(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Update(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(400, res.StatusCode);
            Assert.IsFalse(obj.Ok);
        }

        [Test]
        public void Then_Data_Is_Not_Ok_Update_Return_HTTPCODE_500()
        {

            var data = new ResponseModel<bool>();
            data.AddSystemErrors("Error para test.");

            var iMovimientoService = new Mock<IMovimientoService>();
            iMovimientoService.Setup(p => p.Update(It.IsAny<MovimientoModel>())).Returns(data);

            var controller = new MovimientoController(iMovimientoService.Object);

            var res = controller.Update(It.IsAny<MovimientoModel>()) as ObjectResult;

            var obj = res.Value as ResponseModel<bool>;

            Assert.AreEqual(500, res.StatusCode);

            Assert.IsFalse(obj.Ok);

        }


    }
}
