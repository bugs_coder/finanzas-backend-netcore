﻿using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Services
{
    public class MovimientoService : IMovimientoService
    {
        public MovimientoService(
            IBaseRepository<Movimiento,MovimientoModel> movimientoRepository,
            IBaseRepository<Cuenta, CuentaModel> cuentaRepository, 
            IValidator<MovimientoModel> validator)
        {
            _movimientoRepository = movimientoRepository;
            _cuentaRepository = cuentaRepository;
            _validator = validator;
        }

        private readonly IBaseRepository<Movimiento, MovimientoModel> _movimientoRepository;
        private readonly IBaseRepository<Cuenta, CuentaModel> _cuentaRepository;
        private readonly IValidator<MovimientoModel> _validator;

        public ResponseModel<bool> Delete(int id)
        {
            var res = new ResponseModel<bool>();
            try
            {
                res.Object = _movimientoRepository.Delete(id);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<IEnumerable<MovimientoModel>> GetAll()
        {
            var res = new ResponseModel<IEnumerable<MovimientoModel>>();
            try
            {
                res.Object = _movimientoRepository.GetAll();
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<MovimientoModel> GetById(int id)
        {
            var res = new ResponseModel<MovimientoModel>();
            try
            {
                res.Object = _movimientoRepository.GetById(id);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<bool> Insert(MovimientoModel movimiento)
        {

            var res = new ResponseModel<bool>();
            res.Object = true;
            try
            {
                // validacion del modelo
                var validations = _validator.Validate(movimiento);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                        res.Object = false;
                    }
                else
                {
                    // buscar la cuenta
                    var cuenta = _cuentaRepository.GetById(movimiento.IdCuenta);

                    // si no es credito insertamos de una
                    if (!cuenta.EsTarjetaCredito)
                    {
                        res.Object = _movimientoRepository.Insert(movimiento);
                    }
                    else
                    {
                        // si es credito, dividimos en monto

                        var valorCueta = movimiento.Monto / movimiento.CatidadCuotas;
                        movimiento.Monto = valorCueta;
                        for (int i = 1; i <= movimiento.CatidadCuotas; i++)
                        {
                            movimiento.FechaCarga = DateTime.Today.AddMonths(i);
                            res.Object &= _movimientoRepository.Insert(movimiento);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
                res.Object = false;
            }
            return res;
        }

        public ResponseModel<bool> Update(MovimientoModel movimiento)
        {
            var res = new ResponseModel<bool>();
            try
            {
                var validations = _validator.Validate(movimiento);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                    }
                else
                    res.Object = _movimientoRepository.Update(movimiento);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }
    }
}
