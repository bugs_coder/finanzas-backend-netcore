﻿using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Services
{
    public class CategoriaService : ICategoriaService
    {

        public CategoriaService(IBaseRepository<Categoria,CategoriaModel> categoriaRepository, IValidator<CategoriaModel> validator)
        {
            _categoriaRepository = categoriaRepository;
            _validator = validator;
        }

        private readonly IBaseRepository<Categoria, CategoriaModel> _categoriaRepository;

        private readonly IValidator<CategoriaModel> _validator;

        public ResponseModel<bool> Delete(int id)
        {
            var res = new ResponseModel<bool>();
            try
            {
                res.Object = _categoriaRepository.Delete(id);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<IEnumerable<CategoriaModel>> GetActives()
        {
            var res = new ResponseModel<IEnumerable<CategoriaModel>>();
            try
            {
                res.Object = _categoriaRepository.GetWhere(p=>p.Habilitada);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<IEnumerable<CategoriaModel>> GetAll()
        {
            var res = new ResponseModel<IEnumerable<CategoriaModel>>();
            try
            {
                res.Object = _categoriaRepository.GetAll();

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<CategoriaModel> GetById(int id)
        {
            var res = new ResponseModel<CategoriaModel>();
            try
            {
                res.Object = _categoriaRepository.GetById(id);

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<bool> Insert(CategoriaModel categoria)
        {
            var res = new ResponseModel<bool>();
            try
            {
                var validations = _validator.Validate(categoria);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                    }
                else
                    res.Object = _categoriaRepository.Insert(categoria);

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<bool> Update(CategoriaModel categoria)
        {
            var res = new ResponseModel<bool>();
            try
            {
                var validations = _validator.Validate(categoria);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                    }
                else
                    res.Object = _categoriaRepository.Update(categoria);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }
    }
}
