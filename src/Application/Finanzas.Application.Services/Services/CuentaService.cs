﻿using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Services
{
    public class CuentaService : ICuentaService
    {

        public CuentaService(IBaseRepository<Cuenta,CuentaModel> cuentaRepository, IValidator<CuentaModel> validator)
        {
            _cuentaRepository = cuentaRepository;
            _validator = validator;
        }

        private readonly IBaseRepository<Cuenta, CuentaModel> _cuentaRepository;

        private readonly IValidator<CuentaModel> _validator;

        public ResponseModel<bool> Delete(int id)
        {
            var res = new ResponseModel<bool>();
            try
            {
                res.Object = _cuentaRepository.Delete(id);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<IEnumerable<CuentaModel>> GetActives()
        {
            var res = new ResponseModel<IEnumerable<CuentaModel>>();
            try
            {
                res.Object = _cuentaRepository.GetWhere(p => p.Habilitada);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<IEnumerable<CuentaModel>> GetAll()
        {
            var res = new ResponseModel<IEnumerable<CuentaModel>>();
            try
            {
                res.Object = _cuentaRepository.GetAll();

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<CuentaModel> GetById(int id)
        {
            var res = new ResponseModel<CuentaModel>();
            try
            {
                res.Object = _cuentaRepository.GetById(id);

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<bool> Insert(CuentaModel categoria)
        {
            var res = new ResponseModel<bool>();
            try
            {
                var validations = _validator.Validate(categoria);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                    }
                else
                    res.Object = _cuentaRepository.Insert(categoria);

            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }

        public ResponseModel<bool> Update(CuentaModel categoria)
        {
            var res = new ResponseModel<bool>();
            try
            {
                var validations = _validator.Validate(categoria);
                if (!validations.IsValid)
                    foreach (var error in validations.Errors)
                    {
                        res.AddBussinessErrors(error.ErrorMessage);
                    }
                else
                    res.Object = _cuentaRepository.Update(categoria);
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
            }
            return res;
        }
    }
}
