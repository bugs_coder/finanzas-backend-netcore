﻿using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Services
{
    public class TipoMovimientoService : ITipoMovimientoService
    {

        private readonly IBaseRepository<TiposMovimiento, TiposMovimientoModel> _tipoMovimientoRepository;
        public TipoMovimientoService(IBaseRepository<TiposMovimiento,TiposMovimientoModel> tipoMovimientoRepository)
        {
            _tipoMovimientoRepository = tipoMovimientoRepository;
        }

        public ResponseModel<IEnumerable<TiposMovimientoModel>> GetActives()
        {
            var res = new ResponseModel<IEnumerable<TiposMovimientoModel>>();
            try
            {
                res.Object = _tipoMovimientoRepository.GetWhere(p => p.Habilitado);
                return res;
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
                return res;
            }
        }


        public ResponseModel<IEnumerable<TiposMovimientoModel>> GetAll()
        {

            var res = new ResponseModel<IEnumerable<TiposMovimientoModel>>();
            try
            {
                res.Object = _tipoMovimientoRepository.GetAll();
                return res;
            }
            catch (Exception ex)
            {
                res.AddSystemErrors(ex.ToString());
                return res;
            }
        }

    }
}
