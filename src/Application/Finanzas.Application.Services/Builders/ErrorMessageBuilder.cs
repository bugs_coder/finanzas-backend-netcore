﻿using Finanzas.Domain.Interfaces.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Application.Services.Builders
{
    public class ErrorMessageBuilder : IErrorMessageBuilder
    {
        private string _code;
        private string _message;

        public string Build()
        {
            return $"{_code}{_message}.";
        }

        public IErrorMessageBuilder WithErrorCode(string code)
        {
            _code = code;
            return this;
        }

        public IErrorMessageBuilder WithErrorMessage(string message, params string[] info)
        {
            _message = string.Format(message, info);
            return this;
        }
    }
}
