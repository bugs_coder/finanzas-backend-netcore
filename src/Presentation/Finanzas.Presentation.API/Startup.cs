using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.AspNetCore;
using Finanzas.Intrastructure.EFCore.Context;
using Microsoft.EntityFrameworkCore;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Application.Services.Services;
using Finanzas.Domain.Interfaces.Services;
using FluentValidation;
using Finanzas.Intrastructure.FluentAPI.Validators;
using Finanzas.Domain.Models;
using Finanzas.Intrastructure.Repositories.Repositories;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Application.Services.Builders;
using Finanzas.Intrastructure.Configuratios.Configurations;

namespace Finanzas.Presentation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Finanzas.Presentation.API", Version = "v1" });
            });


            services.RegisterAutoMapper();
            services.RegisterBuilders();
            services.RegisterDbContext(Configuration);
            services.RegisterRepositories();
            services.RegisterServices();
            services.RegisterValidators();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Finanzas.Presentation.API v1"));
            }

            app.UseHttpsRedirection();


            app.UseCors();

            app.UseRouting();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
