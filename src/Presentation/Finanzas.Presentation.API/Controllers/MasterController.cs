﻿using Finanzas.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        public IActionResult GetResponse<T>( ResponseModel<T> response )
        {
            if (response.Errors.Any(p => p.ErrorType == ErrorTypeEmun.System))
                return StatusCode(StatusCodes.Status500InternalServerError, response);

            if (response.Errors.Any(p => p.ErrorType == ErrorTypeEmun.Bussiness))
                return BadRequest(response);

            return Ok(response);
        }



    }
}
