﻿using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoController : MasterController
    {
        private readonly IMovimientoService _movimientoService;

        public MovimientoController(IMovimientoService movimientoService)
        {
            _movimientoService = movimientoService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var res = _movimientoService.GetAll();
            return GetResponse(res);
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var res = _movimientoService.GetById(id);
            return GetResponse(res);
        }


        [HttpPost]
        [Route("insert")]
        public IActionResult Insert(MovimientoModel movimiento)
        {
            var res = _movimientoService.Insert(movimiento);
            return GetResponse(res);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update(MovimientoModel movimiento)
        {
            var res = _movimientoService.Update(movimiento);
            return GetResponse(res);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            var res = _movimientoService.Delete(id);
            return GetResponse(res);
        }
    }
}
