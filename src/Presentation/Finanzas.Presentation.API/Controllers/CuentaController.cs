﻿using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuentaController : MasterController
    {
        private readonly ICuentaService _cuentaService;
        public CuentaController(ICuentaService cuentaService)
        {
            _cuentaService = cuentaService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var res = _cuentaService.GetAll();
            return GetResponse(res);
        }
        [HttpGet]
        [Route("actives")]
        public IActionResult GetActives()
        {
            var res = _cuentaService.GetActives();
            return GetResponse(res);
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var res = _cuentaService.GetById(id);
            return GetResponse(res);
        }


        [HttpPost]
        [Route("insert")]
        public IActionResult Insert(CuentaModel cuenta)
        {
            var res = _cuentaService.Insert(cuenta);
            return GetResponse(res);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update(CuentaModel cuenta)
        {
            var res = _cuentaService.Update(cuenta);
            return GetResponse(res);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            var res = _cuentaService.Delete(id);
            return GetResponse(res);
        }
    }
}
