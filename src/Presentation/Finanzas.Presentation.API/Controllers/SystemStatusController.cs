﻿using Finanzas.Domain.Models;
using Finanzas.Intrastructure.EFCore.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemStatusController : ControllerBase
    {
        private readonly IHostingEnvironment _environment;
        private readonly DbContext _dbContext;

        public SystemStatusController(IHostingEnvironment environment, FinanzasContext dbContext)
        {
            _environment = environment;
            _dbContext = dbContext;
        }
        public IActionResult Get()
        {
            var json = System.IO.File.ReadAllText("./package.json");
            var jsonsettings = new JsonSerializerOptions();
            jsonsettings.PropertyNameCaseInsensitive = true;
            var data = JsonSerializer.Deserialize<SystemStatusModel>(json, jsonsettings);
            data.Environment= _environment.EnvironmentName;
            data.DataSource = _dbContext.Database.GetDbConnection().ConnectionString;

            return Ok(data);
        }
    }
}
