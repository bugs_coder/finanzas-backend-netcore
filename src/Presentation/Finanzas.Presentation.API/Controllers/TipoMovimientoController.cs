﻿using Finanzas.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController()]
    public class TipoMovimientoController : MasterController
    {
        private readonly ITipoMovimientoService _tipoMovimientoService;
        public TipoMovimientoController(ITipoMovimientoService tipoMovimientoService)
        {
            _tipoMovimientoService = tipoMovimientoService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var res = _tipoMovimientoService.GetAll();
            return GetResponse(res);
        }

        [HttpGet]
        [Route("actives")]
        public IActionResult GetActives()
        {
            var res = _tipoMovimientoService.GetActives();
            return GetResponse(res);
        }
    }
}
