﻿using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finanzas.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : MasterController
    {
        private readonly ICategoriaService _categoriaService;
        public CategoriaController(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var res = _categoriaService.GetAll();
            return GetResponse(res);
        }
        [HttpGet]
        [Route("actives")]
        public IActionResult GetActives()
        {
            var res = _categoriaService.GetActives();
            return GetResponse(res);
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var res = _categoriaService.GetById(id);
            return GetResponse(res);
        }


        [HttpPost]
        [Route("insert")]
        public IActionResult Insert(CategoriaModel categoria)
        {
            var res = _categoriaService.Insert(categoria);
            return GetResponse(res);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update(CategoriaModel categoria)
        {
            var res = _categoriaService.Update(categoria);
            return GetResponse(res);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            var res = _categoriaService.Delete(id);
            return GetResponse(res);
        }
    }
}
