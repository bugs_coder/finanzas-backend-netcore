﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Models;

namespace Finanzas.Intrastructure.AutoMapper.Profiles
{
    public class MovimientoProfile: Profile
    {
        public MovimientoProfile()
        {
            CreateMap<Movimiento, MovimientoModel>()
                .ForMember(p => p.Cuenta, opt => opt.MapFrom(c => c.IdCuentaNavigation.Nombre))
            .ForMember(p => p.Categoria, opt => opt.MapFrom(c => c.IdCategoriaNavigation.Nombre))
            .ForMember(p => p.TipoMovimiento, opt => opt.MapFrom(c => c.IdTipoMovimientoNavigation.Nombre));
            CreateMap<MovimientoModel, Movimiento>();

          
        }

    }
}
