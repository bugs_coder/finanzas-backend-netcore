﻿using AutoMapper;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.AutoMapper.Profiles
{
    public class CuentaProfile : Profile
    {
        public CuentaProfile()
        {
            CreateMap<Cuenta, CuentaModel>();
            CreateMap<CuentaModel, Cuenta>();

        }
    }
}
