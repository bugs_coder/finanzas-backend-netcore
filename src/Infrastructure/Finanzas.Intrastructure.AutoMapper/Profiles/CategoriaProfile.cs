﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Models;

namespace Finanzas.Intrastructure.AutoMapper.Profiles
{
    public class CategoriaProfile :Profile
    {
        public CategoriaProfile()
        {
            CreateMap<Categoria, CategoriaModel>();
            CreateMap<CategoriaModel, Categoria>();

        }
    }
}
