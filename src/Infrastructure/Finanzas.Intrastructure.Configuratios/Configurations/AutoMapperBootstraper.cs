﻿using Finanzas.Intrastructure.AutoMapper.Profiles;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
   public static class AutoMapperBootstraper
    {
        public static IServiceCollection RegisterAutoMapper(this IServiceCollection services)
        {
            //var assamblyAutomapper = System.Reflection.Assembly.Load("Finanzas.Intrastructure.AutoMapper");
            services.AddAutoMapper(typeof(MovimientoProfile));
            return services;
        }

    }
}
