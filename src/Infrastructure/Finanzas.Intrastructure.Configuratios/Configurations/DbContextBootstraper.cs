﻿using Finanzas.Intrastructure.EFCore.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
    public static class DbContextBootstraper
    {
        public static IServiceCollection RegisterDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<FinanzasContext>(opt =>
            {
                opt.UseSqlServer(configuration.GetConnectionString("dbFinanzas"))
                .UseLazyLoadingProxies();
                
            });

            return services;
        }

    }
}
