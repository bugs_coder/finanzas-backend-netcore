﻿using Finanzas.Application.Services.Builders;
using Finanzas.Application.Services.Services;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Finanzas.Intrastructure.FluentAPI.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
    public static class BuildersBootstraper
    {
        public static IServiceCollection RegisterBuilders(this IServiceCollection services)
        {
            services.AddTransient<IErrorMessageBuilder, ErrorMessageBuilder>();
            return services;
        }

    }
}
