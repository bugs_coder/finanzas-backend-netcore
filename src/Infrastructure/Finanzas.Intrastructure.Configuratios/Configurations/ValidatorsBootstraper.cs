﻿using Finanzas.Application.Services.Services;
using Finanzas.Domain.Interfaces.Services;
using Finanzas.Domain.Models;
using Finanzas.Intrastructure.FluentAPI.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
    public static class ValidatorsBootstraper
    {
        public static IServiceCollection RegisterValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<CategoriaModel>, CategoriaValidator>();
            services.AddTransient<IValidator<CuentaModel>, CuentaValidator>();
            services.AddTransient<IValidator<MovimientoModel>, MovimientoValidator>();
            return services;
        }

    }
}
