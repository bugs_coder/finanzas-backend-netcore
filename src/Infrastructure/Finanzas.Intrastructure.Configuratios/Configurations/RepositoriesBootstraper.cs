﻿using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Intrastructure.Repositories.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
   public static class RepositoriesBootstraper
    {
        public static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {
            services.AddTransient(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));
            return services;
        }
    }
}
