﻿using Finanzas.Application.Services.Services;
using Finanzas.Domain.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Configuratios.Configurations
{
    public static class ServicesBootstraper
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<ITipoMovimientoService, TipoMovimientoService>();

            services.AddTransient<ICategoriaService, CategoriaService>();

            services.AddTransient<ICuentaService, CuentaService>();

            services.AddTransient<IMovimientoService, MovimientoService>();

            return services;
        }

    }
}
