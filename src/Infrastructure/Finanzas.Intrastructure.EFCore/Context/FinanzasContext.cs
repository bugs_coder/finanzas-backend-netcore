﻿using System;
using Microsoft.EntityFrameworkCore;
using Finanzas.Domain.Entities;

#nullable disable

namespace Finanzas.Intrastructure.EFCore.Context
{
    public partial class FinanzasContext : DbContext
    {
        public FinanzasContext()
        {
        }

        public FinanzasContext(DbContextOptions<FinanzasContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<Cuenta> Cuentas { get; set; }
        public virtual DbSet<Movimiento> Movimientos { get; set; }
        public virtual DbSet<TiposMovimiento> TiposMovimientos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AI");

            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.Property(e => e.IdTipoMovimiento).HasColumnName("Id_Tipo_Movimiento");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTipoMovimientoNavigation)
                    .WithMany(p => p.Categoria)
                    .HasForeignKey(d => d.IdTipoMovimiento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Categorias_TiposMovimientos");
            });

            modelBuilder.Entity<Cuenta>(entity =>
            {
                entity.Property(e => e.DiaCierre).HasColumnName("Dia_Cierre");

                entity.Property(e => e.DiaVencimiento).HasColumnName("Dia_Vencimiento");

                entity.Property(e => e.EsTarjetaCredito).HasColumnName("Es_Tarjeta_Credito");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Movimiento>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCarga).HasColumnName("Fecha_Carga");

                entity.Property(e => e.IdCategoria).HasColumnName("Id_Categoria");

                entity.Property(e => e.IdCuenta).HasColumnName("Id_Cuenta");

                entity.Property(e => e.IdTipoMovimiento).HasColumnName("Id_Tipo_Movimiento");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdCategoria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Categorias");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdCuenta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Cuentas");

                entity.HasOne(d => d.IdTipoMovimientoNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdTipoMovimiento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_TiposMovimientos");
            });

            modelBuilder.Entity<TiposMovimiento>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
