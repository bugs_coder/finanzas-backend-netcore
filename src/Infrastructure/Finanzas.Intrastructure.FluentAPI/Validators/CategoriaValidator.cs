﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finanzas.Domain.Constants;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Models;
using FluentValidation;

namespace Finanzas.Intrastructure.FluentAPI.Validators
{
    public class CategoriaValidator : AbstractValidator<CategoriaModel>
    {

        public CategoriaValidator(IErrorMessageBuilder errorMessageBuilder, IBaseRepository<TiposMovimiento, TiposMovimientoModel> tiposMovimientoRepository)
        {
            RuleFor(p => p.Id)
                .GreaterThanOrEqualTo(0);
            RuleFor(p => p.Nombre)
                .MinimumLength(10)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MINIMO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MINIMO, nameof(p.Nombre), "10")
                            .Build())
                .NotNull()
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_NOTNULL)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_NOTNULL, nameof(p.Nombre))
                            .Build())
                .MaximumLength(150)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAXIMO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAXIMO, nameof(p.Nombre), "150")
                            .Build())
                .NotEmpty()
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_EMPTY)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_EMPTY, nameof(p.Nombre))
                            .Build());

            RuleFor(p => p.IdTipoMovimiento)
                .GreaterThan(0)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, "tipo de movimiento","0")
                            .Build())
                .Must(IdTipoMovimiento => tiposMovimientoRepository.GetById(IdTipoMovimiento) != null)
                     .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_NO_VALIDO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_NO_VALIDO, "tipo de movimiento")
                            .Build());

        }


    }
}
