﻿using Finanzas.Domain.Constants;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.FluentAPI.Validators
{
    public class CuentaValidator : AbstractValidator<CuentaModel>
    {

        public CuentaValidator(IErrorMessageBuilder errorMessageBuilder)
        {
            RuleFor(p => p.Id)
                .GreaterThanOrEqualTo(0);
            RuleFor(p => p.Nombre)
              .MinimumLength(10)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MINIMO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MINIMO, nameof(p.Nombre), "10")
                            .Build())
                .NotNull()
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_NOTNULL)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_NOTNULL, nameof(p.Nombre))
                            .Build())
                .MaximumLength(150)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAXIMO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAXIMO, nameof(p.Nombre), "150")
                            .Build())
                .NotEmpty()
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_EMPTY)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_EMPTY, nameof(p.Nombre))
                            .Build());


            RuleFor(p => p.DiaCierre)
                .InclusiveBetween(1, 28)
                .When(p=>p.DiaCierre.HasValue)
            .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de cierre", "1", "28")
                            .Build());


            RuleFor(p => p.DiaVencimiento)
                .InclusiveBetween(1, 28)
                .When(p => p.DiaVencimiento.HasValue)
                 .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_INTERVALO_DIAS)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_INTERVALO_DIAS, "dia de vencimiento", "1", "28")
                            .Build());
        }


    }
}
