﻿using Finanzas.Domain.Constants;
using Finanzas.Domain.Entities;
using Finanzas.Domain.Interfaces.Builders;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.FluentAPI.Validators
{
    public class MovimientoValidator : AbstractValidator<MovimientoModel>
    {
        private readonly IBaseRepository<Cuenta, CuentaModel> _cuentaRepository;

        public MovimientoValidator(
            IBaseRepository<Cuenta, CuentaModel> cuentaRepository,
            IBaseRepository<Categoria, CategoriaModel> categoriaRepository,
            IErrorMessageBuilder errorMessageBuilder)
        {

            _cuentaRepository = cuentaRepository;
            RuleFor(p => p.IdCategoria)
                .GreaterThan(0)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, nameof(p.IdCategoria), "0")
                            .Build())
                .Must(idCategoria => categoriaRepository.GetById(idCategoria) != null)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_NO_VALIDO)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_NO_VALIDO, "categoria")
                            .Build());
            RuleFor(p => p.IdCuenta)
                .GreaterThan(0)
                .WithMessage(p => errorMessageBuilder
                        .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                        .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, nameof(p.IdCuenta), "0")
                        .Build())
                .Must(idCuenta => cuentaRepository.GetById(idCuenta) != null)
                .WithMessage(p => errorMessageBuilder
                        .WithErrorCode(ErrorCodesConstant.COD_NO_VALIDO)
                        .WithErrorMessage(ErrorMessagesConstant.MSG_NO_VALIDO, "Cuenta")
                        .Build());

            RuleFor(p => p.IdTipoMovimiento)
                .GreaterThan(0)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, nameof(p.IdTipoMovimiento), "0")
                            .Build());
            RuleFor(p => p.Monto)
                .GreaterThan(0)
                .WithMessage(p => errorMessageBuilder
                        .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                        .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, nameof(p.Monto), "0")
                        .Build());

            RuleFor(p => p.CatidadCuotas)
                .GreaterThan(0)
                .When(p=>p.IdCuenta > 0)
                .When(IsCreditCart)
                .WithMessage(p => errorMessageBuilder
                            .WithErrorCode(ErrorCodesConstant.COD_MAYOR_A)
                            .WithErrorMessage(ErrorMessagesConstant.MSG_MAYOR_A, nameof(p.Monto), "0")
                            .Build());
        }

        private bool IsCreditCart(MovimientoModel arg)
        {
            var cuenta = _cuentaRepository.GetById(arg.IdCuenta);
            return cuenta != null && cuenta.EsTarjetaCredito;
        }
    }
}
