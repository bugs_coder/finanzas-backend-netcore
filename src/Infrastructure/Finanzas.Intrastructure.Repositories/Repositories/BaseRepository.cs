﻿using AutoMapper;
using Finanzas.Domain.Interfaces.Repositories;
using Finanzas.Intrastructure.EFCore.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Intrastructure.Repositories.Repositories
{
    public class BaseRepository<TEntity, TModel> : IBaseRepository<TEntity, TModel> where TEntity : class
    {
        protected  readonly FinanzasContext _dbContext;
        protected readonly IMapper _mapper;

        public BaseRepository(FinanzasContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public bool Delete(int id)
        {
            var dbSet = _dbContext.Set<TEntity>();
            var data = dbSet.Find(id);
            _dbContext.Remove(data);
            return _dbContext.SaveChanges() > 0;
        }

        public IEnumerable<TModel> GetWhere(Func<TEntity, bool> predicate)
        {
            var dbSet = _dbContext.Set<TEntity>();
            var res = dbSet.Where(predicate).ToList();
            return _mapper.Map<IEnumerable<TModel>>(res);
        }

        public IEnumerable<TModel> GetAll()
        {
            var dbSet = _dbContext.Set<TEntity>();
            var res = dbSet.ToList();
            return _mapper.Map<IEnumerable<TModel>>(res);
        }

        public TModel GetById(int id)
        {
            var dbSet = _dbContext.Set<TEntity>();
            var res = dbSet.Find(id);
            return _mapper.Map<TModel>(res);
        }

        public bool Insert(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);
            var dbSet = _dbContext.Set<TEntity>();
            dbSet.Add(entity);
            return _dbContext.SaveChanges() > 0;
        }

        public bool Update(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);
            var dbSet = _dbContext.Set<TEntity>();
            dbSet.Update(entity);
            return _dbContext.SaveChanges() > 0;
        }
    }
}
