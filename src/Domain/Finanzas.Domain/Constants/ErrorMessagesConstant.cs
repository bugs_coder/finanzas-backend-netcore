﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Constants
{
    public class ErrorMessagesConstant
    {
        public const string MSG_NOTNULL = "El campo {0} no puede ser nulo";
        public const string MSG_MAXIMO = "El campo {0} tiene un maximo de {1}";
        public const string MSG_MINIMO = "El campo {0} tiene un minimo de {1}";
        public const string MSG_EMPTY = "El campo {0} no puede estar vacio";
        public const string MSG_MAYOR_A = "El campo {0} debe ser mayor a {1}";
        public const string MSG_NO_VALIDO = "El campo {0} debe ser valida";
        public const string MSG_INTERVALO_DIAS = "El campo {0} debe ser entre el {1} y el {2} de cada mes";

        

    }
}
