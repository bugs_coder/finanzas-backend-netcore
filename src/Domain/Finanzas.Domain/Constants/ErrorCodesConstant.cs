﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Constants
{
    public class ErrorCodesConstant
    {
        public const string COD_NOTNULL = "CNN-";
        public const string COD_EMPTY = "CET-";
        public const string COD_MAXIMO = "CMX-";
        public const string COD_MINIMO = "CMN-";
        public const string COD_MAYOR_A = "CMY-";
        public const string COD_NO_VALIDO = "CNV-";
        public const string COD_INTERVALO_DIAS = "CIV-";
    }
}
