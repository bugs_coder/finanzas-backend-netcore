﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Models
{
    public class MovimientoModel
    {
        public int Id { get; set; }
        public DateTime FechaCarga { get; set; }
        public double Monto { get; set; }
        public string Descripcion { get; set; }
        public int CatidadCuotas { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdCategoria { get; set; }
        public string Cuenta { get; set; }
        public string Categoria { get; set; }
        public string TipoMovimiento { get; set; }

    }
}
