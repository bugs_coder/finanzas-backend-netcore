﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Models
{
    public class SystemStatusModel
    {
        public string Version { get; set; }
        public string Name { get; set; }
        public string Environment { get; set; }
        public string DataSource { get; set; }
    }
}
