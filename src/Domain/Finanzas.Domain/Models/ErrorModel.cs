﻿namespace Finanzas.Domain.Models
{
    public class ErrorModel
    {
        public ErrorModel(string message, ErrorTypeEmun errorType)
        {
            Message = message;
            ErrorType = errorType;
        }

        public string Message { get; }
        public ErrorTypeEmun ErrorType { get; }
    }
}