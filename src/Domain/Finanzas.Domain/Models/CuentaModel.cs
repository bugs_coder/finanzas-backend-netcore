﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Models
{
    public class CuentaModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool EsTarjetaCredito { get; set; }
        public int? DiaCierre { get; set; }
        public int? DiaVencimiento { get; set; }
        public bool Habilitada { get; set; }
    }
}
