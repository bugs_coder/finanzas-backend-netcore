﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Models
{
    public class ResponseModel<T>
    {

        public ResponseModel()
        {
            Errors = new List<ErrorModel>();
        }

        public bool Ok { get { return !Errors.Any(); } }
        public T Object { get; set; }

        public string Message { get; set; }
        public string ErrorsMessages { get { return string.Join(". ", Errors.Select(p => p.Message)); } }

        public List<ErrorModel> Errors;

        private void AddErrors(string message, ErrorTypeEmun errorType)
        {
            Errors.Add(new ErrorModel(message, errorType));
        }

        public void AddBussinessErrors(string message)
        {
            AddErrors(message , ErrorTypeEmun.Bussiness);
        }

        public void AddSystemErrors(string message)
        {
            AddErrors(message, ErrorTypeEmun.System);
        }

    }
}
