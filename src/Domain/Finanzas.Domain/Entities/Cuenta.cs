﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Finanzas.Domain.Entities
{
    public partial class Cuenta
    {
        public Cuenta()
        {
            Movimientos = new HashSet<Movimiento>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool EsTarjetaCredito { get; set; }
        public int? DiaCierre { get; set; }
        public int? DiaVencimiento { get; set; }
        public bool Habilitada { get; set; }
        public virtual ICollection<Movimiento> Movimientos { get; set; }
    }
}
