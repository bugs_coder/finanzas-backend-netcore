﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Finanzas.Domain.Entities
{
    public partial class TiposMovimiento
    {
        public TiposMovimiento()
        {
            Categoria = new HashSet<Categoria>();
            Movimientos = new HashSet<Movimiento>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Habilitado { get; set; }

        public virtual ICollection<Categoria> Categoria { get; set; }
        public virtual ICollection<Movimiento> Movimientos { get; set; }
    }
}
