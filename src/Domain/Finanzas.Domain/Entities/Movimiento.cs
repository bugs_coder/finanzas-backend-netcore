﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Finanzas.Domain.Entities
{
    public partial class Movimiento
    {
        public int Id { get; set; }
        public DateTime FechaCarga { get; set; }
        public double Monto { get; set; }
        public string Descripcion { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdCategoria { get; set; }

        public virtual Categoria IdCategoriaNavigation { get; set; }
        public virtual Cuenta IdCuentaNavigation { get; set; }
        public virtual TiposMovimiento IdTipoMovimientoNavigation { get; set; }
    }
}
