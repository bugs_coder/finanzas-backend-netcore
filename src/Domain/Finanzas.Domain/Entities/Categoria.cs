﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Finanzas.Domain.Entities
{
    public partial class Categoria
    {
        public Categoria()
        {
            Movimientos = new HashSet<Movimiento>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Habilitada { get; set; }
        public int IdTipoMovimiento { get; set; }

        public virtual TiposMovimiento IdTipoMovimientoNavigation { get; set; }
        public virtual ICollection<Movimiento> Movimientos { get; set; }
    }
}
