﻿using Finanzas.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Interfaces.Services
{
    public interface IMovimientoService
    {
        ResponseModel<IEnumerable<MovimientoModel>> GetAll();
        ResponseModel<MovimientoModel> GetById(int id);
        ResponseModel<bool> Insert(MovimientoModel movimiento);
        ResponseModel<bool> Update(MovimientoModel movimiento);
        ResponseModel<bool> Delete(int id);

    }
}
