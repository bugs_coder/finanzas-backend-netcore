﻿using Finanzas.Domain.Models;
using System.Collections.Generic;

namespace Finanzas.Domain.Interfaces.Services
{
    public interface ITipoMovimientoService
    {
        ResponseModel<IEnumerable<TiposMovimientoModel>> GetAll();
        ResponseModel<IEnumerable<TiposMovimientoModel>> GetActives();
    }
}