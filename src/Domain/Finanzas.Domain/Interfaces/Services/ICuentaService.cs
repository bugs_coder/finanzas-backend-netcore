﻿using Finanzas.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Interfaces.Services
{
    public interface ICuentaService
    {
        ResponseModel<IEnumerable<CuentaModel>> GetAll();
        ResponseModel<IEnumerable<CuentaModel>> GetActives();
        ResponseModel<CuentaModel> GetById(int id);
        ResponseModel<bool> Insert(CuentaModel categoria);
        ResponseModel<bool> Update(CuentaModel categoria);
        ResponseModel<bool> Delete(int id);

    }
}
