﻿using Finanzas.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Interfaces.Services
{
    public interface ICategoriaService
    {
        ResponseModel<IEnumerable<CategoriaModel>> GetAll();
        ResponseModel<IEnumerable<CategoriaModel>> GetActives();
        ResponseModel<CategoriaModel> GetById(int id);
        ResponseModel<bool> Insert(CategoriaModel categoria);
        ResponseModel<bool> Update(CategoriaModel categoria);
        ResponseModel<bool> Delete(int id);

    }
}
