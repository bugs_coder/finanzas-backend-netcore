﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity, TModel>
    {
        IEnumerable<TModel> GetAll();
        IEnumerable<TModel> GetWhere(Func<TEntity, bool> predicate);
        TModel GetById(int id);

        bool Insert(TModel categoria);
        bool Update(TModel categoria);
        bool Delete(int id);
    }
}
