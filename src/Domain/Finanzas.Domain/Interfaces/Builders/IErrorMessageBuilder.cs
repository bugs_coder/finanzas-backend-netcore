﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Domain.Interfaces.Builders
{
    public interface IErrorMessageBuilder
    {
        public IErrorMessageBuilder WithErrorCode(string code);
        public IErrorMessageBuilder WithErrorMessage(string code, params string[] info);
        public string Build();
    }
}
