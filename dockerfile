FROM mcr.microsoft.com/dotnet/sdk:5.0 AS publish
WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish -c Release -r linux-x64 --self-contained false --output "./publish"

FROM scratch
WORKDIR /app
COPY --from=publish /app/publish .